
package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ListView lvContact;
    private List<ContactModel> listContact = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        lvContact = (ListView) findViewById(R.id.lvContact);
        ContactAdapter adapter = new ContactAdapter(listContact,this);
        lvContact.setAdapter(adapter);

        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactModel contactModel = listContact.get(position);
                Toast.makeText(MainActivity.this, contactModel.getName(),Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initData(){
        ContactModel contact = new ContactModel("Giang Thanh Duy", "012312312", R.drawable.ic_user);
        listContact.add(contact);
        contact = new ContactModel("Giang Thanh Duy", "012312312", R.drawable.ic_user);
        listContact.add(contact);
        contact = new ContactModel("Giang Thanh Duy", "012312312", R.drawable.ic_user);
        listContact.add(contact);
        contact = new ContactModel("Giang Thanh Duy", "012312312", R.drawable.ic_user);
        listContact.add(contact);
        contact = new ContactModel("Giang Thanh Duy", "012312312", R.drawable.ic_user);
        listContact.add(contact);
        contact = new ContactModel("Giang Thanh Duy", "012312312", R.drawable.ic_user);
        listContact.add(contact);
    }
}